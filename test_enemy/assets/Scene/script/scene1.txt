在一個遙遠的國度，
一位王子為了向周遭的人證明
自己能夠接下領導國家的大任
便聽從父皇的指令
微服出巡四處
學習身為領導人該有的能力......