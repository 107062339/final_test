
const {ccclass, property} = cc._decorator;
​
@ccclass
export default class TypeWriter extends cc.Component {
​
    @property(cc.Label)
    label: cc.Label = null;

    private text: string = '在一個遙遠的國度，一位王子為了向周遭的人證明自己能夠接下領導國家的大任，便聽從父皇的指令，微服出巡四處，學習身為領導人該有的能力......';
​
    private text2:string = '然而，當王子已學成歸國時，原本美麗的宮殿，竟背大陸上，邪惡、野蠻的生物占據著，眼望著如此慘況，王子無法抑制心中怒火，決心使用一路中所學的戰鬥技巧，為了自己曾寶貴的人們踏上復仇之路......'

    private duration: number = 100;
​
    start () {
        // init logic
        
        let strLen = this.text.length;
        let content = this.text.split("");
        let curStr = "";
        let self = this;

        this.schedule(self.oput, 0.1);

        for(let i = 0; i < strLen; i++){
            setTimeout(()=>{
                curStr += content[i];
                self.label.string = curStr;
            },self.duration*(i));
        }

        content = this.text2.split("");

        for(let i = 0; i < strLen; i++){
            setTimeout(()=>{
                curStr += content[i];
                self.label.string = curStr;
            },self.duration*(i));
        }
    }

    oput(){

    }
}