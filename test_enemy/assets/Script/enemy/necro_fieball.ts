// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class fireball extends cc.Component {

    @property()
    movespeed:number = 100;


    private startpos:cc.Vec2;
    private player:cc.Node;
    private endpos:cc.Vec2;

    private arrow_dir_change;
    private arrow_dir;

    private dir_change;

    


    start () {
        this.player = this.node.parent.getChildByName("player");
        this.startpos = this.node.position;
        this.endpos = this.player.position;

        this.dir_change = ()=>{
            this.endpos = this.player.position;
            this.startpos = this.node.position;

            let dirVec = this.endpos.sub(this.startpos);
            var angle = dirVec.signAngle(cc.v2(1,0));
            let degree = Math.floor(cc.misc.radiansToDegrees(angle));
            
            this.node.rotation = degree-180;

            this.arrow_dir = dirVec;
            this.arrow_dir.normalizeSelf();
        }

        this.arrow_dir = this.endpos.sub(this.startpos);

        let degree = Math.floor(cc.misc.radiansToDegrees(this.arrow_dir.signAngle(cc.v2(1,0))));
        this.node.rotation = degree-180;

        this.arrow_dir.normalizeSelf();

        this.schedule(this.dir_change, 0.5);
    }

     update (dt) {
        this.node.x += dt*this.arrow_dir.x * this.movespeed;
        this.node.y += dt*this.arrow_dir.y * this.movespeed;
        //if(Math.abs(this.node.x) > 480)this.node.destroy();
        //if(Math.abs(this.node.y) > 320)this.node.destroy();
     }
}
