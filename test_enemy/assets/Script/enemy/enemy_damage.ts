// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property()
    life:number = 50;
    @property({type:cc.AudioClip})
    zombie_hurt:cc.AudioClip = null;
    @property({type:cc.AudioClip})
    zombie_die:cc.AudioClip = null;
    @property({type:cc.AudioClip})
    shield_defend:cc.AudioClip = null;
    @property({type:cc.AudioClip})
    shield_hurt:cc.AudioClip = null;
    @property({type:cc.AudioClip})
    archer_die:cc.AudioClip = null;
    @property({type:cc.AudioClip})
    archer_hurt:cc.AudioClip = null;
    @property({type:cc.AudioClip})
    necro_die:cc.AudioClip = null;
    
    @property(cc.Prefab)
    boss_:cc.Prefab = null;

    private boss_bar:cc.Node;
    private boss_life:number;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        if(this.node.name == "ninja" || this.node.name == "necro"){
            this.boss_bar = cc.instantiate(this.boss_);
            this.boss_bar.setParent(cc.find("Canvas/Main Camera"));
            this.boss_life = this.life;
        }
    }

    // update (dt) {}
    
    onBeginContact(contact,self,other){
        if(other.node.group == "bullet"||other.node.group == "axe"||other.node.group == "melee"){
            let a =other.node.getComponent(other.node.group).damage;
            cc.log("get damage "+a, "by "+other.node.name);
            if(this.node.name != "ene_shield"){               
                if(this.node.name == "ene_archer_2" || this.node.name == "ene_archer"){
                    cc.audioEngine.playEffect(this.archer_hurt,false);
                }
                else if(this.node.name == "ene_zombie_1" || this.node.name == "ene_zombie_2"){
                    cc.audioEngine.playEffect(this.zombie_hurt,false);
                }
                else if(this.node.name == "ninja" || this.node.name == "necro"){
                    //let b = a/this.boss_life;
                    this.boss_bar.getChildByName("black").getComponent(cc.Sprite).fillStart -= (a/this.boss_life)
                    if(this.boss_bar.getChildByName("black").getComponent(cc.Sprite).fillStart < 0)
                    this.boss_bar.getChildByName("black").getComponent(cc.Sprite).fillStart = 0;
                }
                this.life -= a;
            }
            else{
                if(self.tag != 1){
                    cc.audioEngine.playEffect(this.shield_hurt,false);
                    this.life -= a;
                }
                else{
                    cc.audioEngine.playEffect(this.shield_defend,false);
                }
            }
            //other.node.destroy();
            if(this.life <= 0){
                if(self.node.name == "ene_zombie_2" || self.node.name == "ene_zombie_1"){
                    cc.audioEngine.playEffect(this.zombie_die,false);
                    
                    if(self.node.name == "ene_zombie_2")
                        this.node.getComponent("ene_zombie_2").dead = true;
                    else
                        this.node.destroy();
                }
                else if(self.node.name == "ene_archer_2" || self.node.name == "ene_archer"){
                    cc.audioEngine.playEffect(this.archer_die,false);
                    
                    if(self.node.name == "ene_archer_2")
                        this.node.getComponent("ene_archer_elite").dead = true;
                    else
                        this.node.destroy();
                }
                else if(self.node.name == "ninja"){
                    let a = cc.find("Canvas").getComponent("data_controller");
                    this.boss_bar.destroy();
                    a.change_scene('stage2');
                    //this.scheduleOnce(()=>{this.node.destroy()},0.01);
                }
                else if(self.node.name == "necro"){
                    let a = cc.find("Canvas").getComponent("data_controller");
                    cc.audioEngine.playEffect(this.necro_die,false);
                    this.boss_bar.destroy();
                    a.change_scene('ed');
                }
                else{         
                    this.node.destroy();
                }
            }
        }else if(other.node.group == "melee"){
        }
    }
}
