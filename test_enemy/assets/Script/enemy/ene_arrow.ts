// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class ene_arrow extends cc.Component {


    // LIFE-CYCLE CALLBACKS:

    @property()
    movespeed:number = 800;

    // onLoad () {}
    //private movespeed:number = 800;

    private startpos:cc.Vec2;
    private player:cc.Node;
    private endpos:cc.Vec2;

    private arrow_dir_change;
    private arrow_dir;

    


    start () {
        this.player = this.node.parent.getChildByName("player");
        this.startpos = this.node.position;
        this.endpos = this.player.position;

        this.arrow_dir = this.endpos.sub(this.startpos);

        let degree = Math.floor(cc.misc.radiansToDegrees(this.arrow_dir.signAngle(cc.v2(1,0))));
        this.node.rotation = degree-90;

        this.arrow_dir.normalizeSelf();
    }

     update (dt) {
        this.node.x += dt*this.arrow_dir.x * this.movespeed;
        this.node.y += dt*this.arrow_dir.y * this.movespeed;
        ///if(Math.abs(this.node.x) > 480)this.node.destroy();
        //if(Math.abs(this.node.y) > 320)this.node.destroy();
     }
}
