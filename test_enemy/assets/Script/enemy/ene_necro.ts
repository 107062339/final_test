// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class ene_necro extends cc.Component {

    @property(cc.Prefab)
    fireball:cc.Prefab = null;
    @property(cc.Prefab)
    flash:cc.Prefab = null;
    @property(cc.Prefab)
    zombie:cc.Prefab = null;
    @property(cc.Prefab)
    rock:cc.Prefab = null;

    @property({type:cc.AudioClip})
    effect_fireball:cc.AudioClip = null;
    @property({type:cc.AudioClip})
    effect_summon:cc.AudioClip = null;
    @property({type:cc.AudioClip})
    effect_laugh:cc.AudioClip = null;

    private attack_interval:number = 2;
    private attack_count:number = 0;

    private fireball_node:cc.Node;
    private flash_node:cc.Node;
    private zombie_node:cc.Node;
    private player:cc.Node;
    private rock_node:cc.Node;

    private flash_funct;
    private summon;
    private fire;
    private dir_change;
    private spell_rock;

    private startpos:cc.Vec2;
    private endpos:cc.Vec2;
    private arrow_dir:cc.Vec2;
    private rand_dir:cc.Vec2;
    start () {
        this.player = this.node.parent.getChildByName("player");

        this.dir_change = ()=>{
            this.endpos = this.player.position;
            this.startpos = this.node.position;
            let dirVec = this.endpos.sub(this.startpos);
            var angle = dirVec.signAngle(cc.v2(1,0));
            let degree = Math.floor(cc.misc.radiansToDegrees(angle));
            this.arrow_dir = dirVec;
            this.arrow_dir.normalizeSelf();
            this.node.rotation = degree;
        }
        
        this.fire = ()=>{
            this.fireball_node = cc.instantiate(this.fireball);
            this.fireball_node.setParent(this.node.parent);
            this.fireball_node.setPosition(this.node.position);
            cc.audioEngine.playEffect(this.effect_fireball,false);
            //this.scheduleOnce(this.ModeChange,0.02);
        }
        this.flash_funct = ()=>{
            this.node.opacity = 0;
            this.flash_node = cc.instantiate(this.flash);
            this.flash_node.setParent(this.node.parent);
            this.flash_node.setPosition(this.node.position.x,this.node.position.y+70);

            let r = this.random(4);
            if(r == 0){
                this.rand_dir = cc.v2(0,220);
            }
            else if(r == 1){
                this.rand_dir = cc.v2(0,-220);
            }
            else if(r == 2){
                this.rand_dir = cc.v2(357,0);
            }
            else if(r == 3){
                this.rand_dir = cc.v2(-357,0);
            }
            this.node.x = this.rand_dir.x
            this.node.y =  this.rand_dir.y
            this.scheduleOnce(()=>{
                //this.node.x = this.rand_dir.x
                //this.node.y =  this.rand_dir.y
                this.node.opacity = 255;
                this.flash_node = cc.instantiate(this.flash);
                this.flash_node.setParent(this.node.parent);
                this.flash_node.setPosition(this.node.position.x,this.node.position.y+70);
                this.ModeChange();
            },1)
            this.summon = ()=>{
                cc.audioEngine.playEffect(this.effect_laugh,false);
                this.schedule(()=>{
                    this.zombie_node =cc.instantiate(this.zombie);
                    this.zombie_node.setParent(this.node.parent);
                    this.zombie_node.setPosition(this.node.position);
                },3,2,0.1);
                this.scheduleOnce(this.ModeChange,10);
            }
        }
        this.spell_rock = ()=>{
            this.rock_node = cc.instantiate(this.rock);
            this.rock_node.setParent(this.node.parent);
            this.rock_node.setPosition(this.player.position);
            this.ModeChange();
        }

        this.scheduleOnce(this.spell_rock, 2);
        //this.ModeChange();
        this.schedule(this.dir_change, 0.1);
    }
    random(randmax:number){
        let a = Math.floor(Math.random()*randmax);
        return a;
    }
    ModeChange(){
        let a = this.random(10);
        //cc.log(a);
        if(this.attack_count == 15){
            this.attack_count = 0;
            this.scheduleOnce(this.summon,this.attack_interval);
        }
        else if(a < 2){
            this.scheduleOnce(this.flash_funct,this.attack_interval);
            this.attack_count += 1;
        }
        else if(a < 8){
            this.scheduleOnce(this.flash_funct,this.attack_interval-1);
            this.scheduleOnce(this.spell_rock,this.attack_interval+1.5-1);
            this.attack_count += 1;
        }
        else{
            this.scheduleOnce(this.flash_funct,this.attack_interval-1);
            this.scheduleOnce(this.fire,this.attack_interval+1.5-1);
            this.attack_count += 1;
        }
    }

    // update (dt) {}
}
