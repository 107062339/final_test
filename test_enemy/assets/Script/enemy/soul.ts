// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    ene_archer_2:cc.Prefab = null;

    public interval:number = 10;
    public dead:boolean = false;
    public reborn:boolean = false;

    private archer:cc.Node;



    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.node.getComponent(cc.Animation).play("soul");
        


        this.scheduleOnce(()=>{
            this.archer = cc.instantiate(this.ene_archer_2);
            this.archer.setParent(this.node.parent);
            this.archer.setPosition(this.node.position);
            this.node.destroy();
        },this.interval);
    }

}
