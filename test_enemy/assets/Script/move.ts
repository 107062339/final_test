const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    
    @property(cc.Prefab)
    onground_ar: cc.Prefab = null;
    
    @property(cc.Prefab)
    onground_axe: cc.Prefab = null;
    
    @property(cc.Prefab)
    onground_gun: cc.Prefab = null;
    
    @property(cc.Prefab)
    onground_knife: cc.Prefab = null;
    
    @property(cc.Prefab)
    onground_samura: cc.Prefab = null;
    
    @property(cc.Prefab)
    onground_shield: cc.Prefab = null;
    
    @property(cc.Prefab)
    onground_shotgum: cc.Prefab = null;
    
    @property(cc.Prefab)
    onground_sniper: cc.Prefab = null;
    
    @property(cc.Prefab)
    onground_lasergun: cc.Prefab = null;

    @property(cc.Prefab)
    onground_grenade: cc.Prefab = null;

    //todo: add 拿武器prefabs
    @property(cc.Prefab)
    holding_ar: cc.Prefab = null;
    
    @property(cc.Prefab)
    holding_axe: cc.Prefab = null;
    
    @property(cc.Prefab)
    holding_gun: cc.Prefab = null;
    
    @property(cc.Prefab)
    holding_knife: cc.Prefab = null;
    
    @property(cc.Prefab)
    holding_samura: cc.Prefab = null;
    
    @property(cc.Prefab)
    holding_shield: cc.Prefab = null;
    
    @property(cc.Prefab)
    holding_shotgum: cc.Prefab = null;
    
    @property(cc.Prefab)
    holding_sniper: cc.Prefab = null;

    @property(cc.Prefab)
    holding_lasergun: cc.Prefab = null;

    @property(cc.Prefab)
    holding_grenade: cc.Prefab = null;

    private hand_l;
    private hand_r; 
    private mw:boolean = false;
    private ms:boolean = false;
    private ma:boolean = false;
    private md:boolean = false;
    private pick:boolean = false;
    public weapon1: string = "fist";
    public weapon2: string = "fist";
    public aimol1: number = 0;
    public aimol2: number = 0;
    public Aimol1: number=30;
    public Aimol2: number=30;
    private curWeapon;
    private attack;

    private movespeed:number = 150;
    private weaponUI = null;

    // LIFE-CYCLE CALLBACKS:

    start () {setTimeout(()=>{
        this.hand_l = this.node.getChildByName("hand_2_l");
        this.hand_r = this.node.getChildByName("hand_2_r");
        this.attack = this.node.getComponent("attack");
        this.weaponUI = cc.find("Canvas/Main Camera").getChildByName("weaponUI").getComponent("weaponUI");
    },100);
        this.hand_l = this.node.getChildByName("hand_2_l");
        this.hand_r = this.node.getChildByName("hand_2_r");
        this.attack = this.node.getComponent("attack");
        this.weaponUI = cc.find("Canvas/Main Camera").getChildByName("weaponUI").getComponent("weaponUI");

        cc.director.getCollisionManager().enabled = true;
        cc.director.getPhysicsManager().enabled = true; 
        document.getElementById("Cocos2dGameContainer").style.cursor = "none";
    }

    rotate(degree:number){
        this.node.rotation = degree;
    
    }

    movedir(dir){
        if(dir == 'w'){
            this.mw = true;
        }
        else if(dir == 's'){
            this.ms = true;
        }
        else if(dir == 'a'){
            this.ma = true;
        }
        else if(dir == 'd'){
            this.md = true;
        }
    }

    movedir_b(dir){
        if(dir == 'w'){
            this.mw = false;
        }
        else if(dir == 's'){
            this.ms = false;
        }
        else if(dir == 'a'){
            this.ma = false;
        }
        else if(dir == 'd'){
            this.md = false;
        }
    }

    Pick(){
        this.pick = true;
        //cc.log(this.pick);
        //cc.log(this.weapon1, this.weapon2);
    }

    Unpick(){
        this.pick = false;
        //cc.log("space is released.");
    }

    getWeapon(weapon:string, itemPos, tempAimol:number){
        if(this.weapon1=="fist"){
            this.takeWeapon(weapon);
            this.weapon1 = weapon;
            this.aimol1 = tempAimol;
            //cc.log(this.weapon1);
        }else if(this.weapon2=="fist"){
            this.weapon2 = weapon;
            this.aimol2 = tempAimol;
            //cc.log(this.weapon2);
        }else{
            this.dropWeapon(itemPos, this.aimol1);
            this.takeWeapon(weapon);
            this.weapon1 = weapon;
            this.aimol1 = tempAimol;
        }
        this.weaponUI.setUI();
    }

    drop(itemPos){
        //cc.log(itemPos);
        if(itemPos!=null)this.dropWeapon(itemPos, this.aimol1);
        if(this.weapon1!="fist") {
            this.curWeapon.destroy();
        }
        this.weapon1 = "fist";
        this.aimol1 = 0;
        this.hand_l.x = 8;
        this.hand_l.y = 25;
        this.hand_r.x = 8;
        this.hand_r.y = -25;
        this.weaponUI.setUI();
    }

    dropWeapon(itemPos, aimol:number){
        //dropweapon
            //cc.log(this.weapon1);
            if(this.weapon1 == "fist"){
                return;
            }else if(this.weapon1 == "axe"){
                var newNode = cc.instantiate(this.onground_axe);
            }else if(this.weapon1 == "ar"){
                var newNode = cc.instantiate(this.onground_ar);
            }else if(this.weapon1 == "lasergun"){
                var newNode = cc.instantiate(this.onground_lasergun);
            }else if(this.weapon1 == "gun"){
                var newNode = cc.instantiate(this.onground_gun);
            }else if(this.weapon1 == "knife"){
                var newNode = cc.instantiate(this.onground_knife);
            }else if(this.weapon1 == "samura"){
                var newNode = cc.instantiate(this.onground_samura);
            }else if(this.weapon1 == "shield"){
                var newNode = cc.instantiate(this.onground_shield);
            }else if(this.weapon1 == "shotgum"){
                var newNode = cc.instantiate(this.onground_shotgum);
            }else if(this.weapon1 == "sniper"){
                var newNode = cc.instantiate(this.onground_sniper);
            }else{}
            newNode.parent = this.node.parent;
            newNode.setPosition(itemPos.x, itemPos.y);
            //cc.log(this.node.x, this.node.y);
            //cc.log(itemPos.x, itemPos.y);
            newNode.getComponent("PickWeapon").SetAimol(aimol);
            //cc.log(itemPos);
            //cc.log(this.node.x, this.node.y);
            //cc.log("drop "+this.weapon1);
            //cc.log(newNode.x, newNode.y)
    }

    swapWeapon(){
        cc.log(this.weapon1, this.weapon2);
        if(this.weapon1== "fist"&&this.weapon2=="fist"){
        }else if(this.weapon1=="fist"){
            this.takeWeapon(this.weapon2);
            this.weapon1 = this.weapon2;
            this.weapon2 = "fist";
        }else if(this.weapon2=="fist"){
            this.takeWeapon(this.weapon2);
            this.weapon2 = this.weapon1;
            this.weapon1 = "fist";
        }else{
            this.takeWeapon(this.weapon2);
            let temp = this.weapon1;
            this.weapon1 = this.weapon2;
            this.weapon2 = temp
            ;}
        let temp1 = this.aimol1;
        this.aimol1 = this.aimol2;
        this.aimol2 = temp1;
        let temp2 = this.Aimol1;
        this.Aimol1 = this.Aimol2;
        this.Aimol2 = temp2;
        //cc.log(this.weapon1, this.weapon2);
        //cc.log(this.aimol1, this.Aimol1, this.aimol2, this.Aimol2);
        cc.log("setUI1");
        this.weaponUI.setUI();
        cc.log("setUI2");
    }

    takeWeapon(weapon:string){
        //cc.log(weapon);
        //this.weaponUI = cc.find("Canvas/Main Camera").getChildByName("weaponUI").getComponent("weaponUI");
        this.hand_l = this.node.getChildByName("hand_2_l");
        this.hand_r = this.node.getChildByName("hand_2_r");
        this.hand_l.rotation = 0;
        this.hand_r.rotation = 0;
        console.log(this.hand_l, this.hand_r);

        this.hand_l.stopAllActions();
        this.hand_r.stopAllActions();
        if(this.weapon1 != "fist"&&cc.isValid(this.curWeapon)) {
            //cc.log(this.weapon1, this.curWeapon);
            this.curWeapon.destroy();
            //cc.log(this.weapon1, this.curWeapon);
        }
        let x = 0, y = 0;
        this.hand_l.x = 8;
        this.hand_l.y = 25;
        this.hand_r.x = 8;
        this.hand_r.y = -25;
        //todo: set weapon position based on character
        if(weapon == "fist"){ 
            return;
        }else if(weapon == "axe"){
            var newNode = cc.instantiate(this.holding_axe);
            x = 20;
            y = -18;
            newNode.scaleX = -1;
        }else if(weapon == "ar"){
            var newNode = cc.instantiate(this.holding_ar);
            this.hand_l.x = 24;
            this.hand_l.y = 1;
            this.hand_r.x = 24;
            this.hand_r.y = -1;
            x = 55;
            y = 0;
        }else if(weapon == "lasergun"){//may not use
            var newNode = cc.instantiate(this.holding_lasergun);
            this.hand_l.x = 24;
            this.hand_l.y = 1;
            this.hand_r.x = 24;
            this.hand_r.y = -1;
            x = 50;
            y = 0;
        }else if(weapon == "gun"){
            var newNode = cc.instantiate(this.holding_gun);
            this.hand_l.x = 24;
            this.hand_l.y = 1;
            this.hand_r.x = 24;
            this.hand_r.y = -1;
            x = 35;
            y = 0;
        }else if(weapon == "knife"){
            var newNode = cc.instantiate(this.holding_knife);
            this.hand_r.x = 20;
            this.hand_r.y = -11;
            x = 43;
            y = -10;
        }else if(weapon == "shield"){
            var newNode = cc.instantiate(this.holding_shield);
            this.hand_r.x = 20;
            this.hand_r.y = -11;
            x = 32;
            y = -4;
        }else if(weapon == "samura"){
            var newNode = cc.instantiate(this.holding_samura);
            this.hand_r.x = 16;
            this.hand_r.y = 15;
            x = -16;
            y = 50;
        }else if(weapon == "shotgum"){
            var newNode = cc.instantiate(this.holding_shotgum);
            this.hand_l.x = 24;
            this.hand_l.y = 1;
            this.hand_r.x = 24;
            this.hand_r.y = -1;
            x = 50;
            y = 0;
        }else if(weapon == "sniper"){
            var newNode = cc.instantiate(this.holding_sniper);
            this.hand_l.x = 24;
            this.hand_l.y = 1;
            this.hand_r.x = 24;
            this.hand_r.y = -1;
            x = 50;
            y = 0;        
        }else if(weapon == "grenade"){
            var newNode = cc.instantiate(this.holding_grenade);
            this.hand_l.x = 20;
            this.hand_l.y = 15;
            x = this.hand_r.x;
            y = this.hand_r.y;
        }
        //cc.log(this.weapon1, this.curWeapon);
        newNode.parent = this.hand_l.parent;
        newNode.setPosition(x, y);
        this.curWeapon = this.node.getChildByUuid(newNode.uuid);
        //cc.log(this.weapon1, this.curWeapon);
        //cc.log(this.curWeapon.parent.name, this.curWeapon.name, this.curWeapon.x, this.curWeapon.y);
        if(cc.isValid(this.weaponUI))this.weaponUI.setUI();
    }

    reloadAimol(){
        this.attack = this.node.getComponent("attack");
        if(!cc.isValid(this.attack))return;
        this.attack.attackInit();
        var reloadAmo = 0;
        var maxAmo = 0;
        if(this.weapon1!="fist"){
            if(this.weapon1 == "gun"){
                maxAmo = 6;
                if(maxAmo-this.aimol1>0&&this.Aimol1>0){//6 for max aimol for this weapon
                    reloadAmo = maxAmo - this.aimol1;
                }
            }else if(this.weapon1 == "shotgum"){
                maxAmo = 5;
                if(maxAmo-this.aimol1>0&&this.Aimol1>0){//6 for max aimol for this weapon
                    reloadAmo = maxAmo - this.aimol1;
                }
            }else if(this.weapon1 == "sniper"){
                maxAmo = 1;
                if(maxAmo-this.aimol1>0&&this.Aimol1>0){//6 for max aimol for this weapon
                    reloadAmo = maxAmo - this.aimol1;
                }
            }else if(this.weapon1 == "firegum"){
                maxAmo = 1;
                if(maxAmo-this.aimol1>0&&this.Aimol1>0){//6 for max aimol for this weapon
                    reloadAmo = maxAmo - this.aimol1;
                }
            }else if(this.weapon1 == "ar"){
                maxAmo = 15;
                if(maxAmo-this.aimol1>0&&this.Aimol1>0){//6 for max aimol for this weapon
                    reloadAmo = maxAmo - this.aimol1;
                }
            }else if(this.weapon1 == "lasergun"){
                maxAmo = 30;
                if(maxAmo-this.aimol1>0&&this.Aimol1>0){//6 for max aimol for this weapon
                    reloadAmo = maxAmo - this.aimol1;
                }
            }
            var tempAimol1 = (this.Aimol1>=reloadAmo)? this.Aimol1 - reloadAmo : 0;
            var tempaimol1 = (this.Aimol1>=reloadAmo)? maxAmo: this.Aimol1+this.aimol1;
        }
        if(reloadAmo!=0&&maxAmo!=0){
            this.attack.beReloading(this.weapon1);
            this.Aimol1 = tempAimol1;
            this.aimol1 = tempaimol1;
        }
        
        this.weaponUI.setUI();
    }

    consumeAimol(aimol:number){
            this.aimol1 -= aimol;
            this.weaponUI.setUI();

    }

    checkAimol(){
        //cc.log("aimol1: "+this.aimol1, ";Aimol2: "+this.Aimol1, ";aimol2: "+this.aimol2, ";Aimol2: "+this.Aimol2);
        return this.Aimol1;
    }

    checkAimol1(){
        //cc.log("aimol1: "+this.aimol1, ";Aimol2: "+this.Aimol1, ";aimol2: "+this.aimol2, ";Aimol2: "+this.Aimol2);
        return this.aimol1;
    }

    checkWeapon(){
        return (cc.isValid(this.weapon1))?this.weapon1:"fist";
    }

    fillAimol(){
        this.Aimol1 = (this.Aimol1+15>=60)? 60 : this.Aimol1 + 15;
        this.weaponUI.setUI();
    }

    determineMovespeed(){
        if(this.weapon1 == "axe"){
            this.movespeed = 175;
        }else if(this.weapon1 == "ar"){
            this.movespeed = 175;
        }else if(this.weapon1 == "lasergun"){
            this.movespeed = 150;
        }else if(this.weapon1 == "gun"){
            this.movespeed = 200;
        }else if(this.weapon1 == "knife"){
            this.movespeed = 200;
        }else if(this.weapon1 == "samura"){
            this.movespeed = 150;
        }else if(this.weapon1 == "shield"){
            this.movespeed = 175;
        }else if(this.weapon1 == "shotgum"){
            this.movespeed = 150;
        }else if(this.weapon1 == "sniper"){
            this.movespeed = 150;
        }else{
            this.movespeed = 225;
        }
    }

    update (dt) {
        if(this.mw) this.node.y += this.movespeed*dt;
        else if(this.ms) this.node.y -= this.movespeed*dt

        if(this.ma) this.node.x -= this.movespeed*dt;
        else if(this.md) this.node.x += this.movespeed*dt;

        this.determineMovespeed();
    }

    onCollisionStay(other,self){
        //cc.log(this.pick);
        if(this.pick){
            //cc.log("pick "+other.node.name);
            if(other.node.name == "aimol") this.fillAimol();
            else this.getWeapon(other.node.name, other.node.position, other.getComponent("PickWeapon").getAimol());
            this.pick = false;
            other.getComponent("PickWeapon").Pick();
        }
    }


}
