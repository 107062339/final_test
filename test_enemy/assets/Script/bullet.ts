const {ccclass, property} = cc._decorator;

@ccclass
export default class bullet extends cc.Component 
{

    @property(cc.Prefab)
    explode: cc.Prefab = null;

    @property(cc.Prefab)
    blood: cc.Prefab = null;

    private anim = null;

    private bulletManager = null;

    public isTriggered = false; // I add this to make the bullet kill one enemy at a time.

    private movespeed:number = 2000;

    private startpos:cc.Vec2;
    private player:cc.Node;
    private endpos:cc.Vec2;
    private diff:cc.Vec2;

    private arrow_dir_change;
    private arrow_dir;
    private aim;
    private degree;
    private x:number=null;
    private y:number=null;
    private isFiring:boolean = false;
    private weapon:cc.Node;
    private damage:number = 0;//todo: 協調傷害
    private attack;


    // when created, the bullet need to be placed at correct position and play animation.
    start(){
        //cc.log("start pos: "+this.startpos);
        //cc.log("end pos: "+this.endpos);
        this.attack = this.node.parent.getChildByName("player").getComponent("attack");//todo: change path;
        //cc.log(this.attack);
    }

    init(player:cc.Node, weapon:cc.Node, aimSize:number, movespeedScalar:number = 1, damage:number){
        this.damage = damage;
        this.movespeed *= movespeedScalar;
        this.weapon = weapon;
        let dirx = Math.floor(Math.random()*2);//x方向
        dirx = (dirx == 1)? 1: -1;
        let diry = Math.floor(Math.random()*2);//y方向
        diry = (diry == 1)? 1: -1;
        let missx= Math.floor(Math.random()*(aimSize/4));//誤差x
        let missy= Math.floor(Math.random()*(aimSize/4));//誤差y
        this.aim = this.node.parent.getChildByName("aim");
        this.startpos = this.node.position;
        console.log(this.startpos, this.endpos, this.arrow_dir);
        let dist = Math.sqrt(weapon.position.x * weapon.position.x + weapon.position.y * weapon.position.y);
        this.x = dist * Math.cos( player.rotation * ( Math.PI / 180));
        this.y = dist * -1 * Math.sin( player.rotation * ( Math.PI / 180));
        this.diff = cc.v2(this.x, this.y);
        this.endpos = this.aim.position;    
        this.endpos.x += dirx*missx;
        this.endpos.y += diry*missy;
        console.log(this.startpos, this.endpos, this.arrow_dir);
        this.arrow_dir = this.endpos.sub(this.startpos);
        this.arrow_dir.normalizeSelf();
        this.degree = cc.misc.radiansToDegrees(this.arrow_dir.signAngle(cc.v2(1,0)));
        this.node.rotation = this.degree;
        this.startpos = player.position.add(this.diff);
        console.log(this.startpos, this.endpos, this.arrow_dir);
        this.node.position = this.startpos;
        this.isFiring = true;
        this.node.getComponent(cc.RigidBody).linearVelocity = this.arrow_dir.mul(this.movespeed).mul(100);
    }

    update (dt) {
        this.node.position = this.node.position;
        if(this.isFiring)
        {
            //this.node.x += dt*this.arrow_dir.x * this.movespeed;
            //this.node.y += dt*this.arrow_dir.y * this.movespeed;
            //cc.log(this.node.rotation);
            if(Math.abs(this.node.x)>640||Math.abs(this.node.y)>640){
                this.attack.setLaserEndPos(this.node.position);
                cc.log(this.node);
                cc.log("laser shoud end:"+this.node.position);
                this.node.destroy();
            }
        }
     }
    
    //detect collision with enemies
    onBeginContact(contact, selfCollider, otherCollider)
    {
        cc.log(selfCollider.node.name, otherCollider.node.name);
        if(this.damage == 0.5){
            this.attack.setLaserEndPos(this.node.position);
            this.node.destroy();
        }else if(otherCollider.node.group == "enemy"){
            var newNode = cc.instantiate(this.blood);
            let dirx = Math.floor(Math.random()*2);
            dirx = (dirx == 1)? 1: -1;
            let diry = Math.floor(Math.random()*2);
            diry = (diry == 1)? 1: -1;
            let posx = Math.floor(Math.random()*25);
            let posy = Math.floor(Math.random()*25);
            let rotation = Math.floor(Math.random()*360);
            let scalar = 0.5 + Math.random()*0.5;
            newNode.setParent(otherCollider.node);
            newNode.setPosition(dirx*posx, diry*posy);
            newNode.setRotation(rotation);
            newNode.setScale(scalar);
            setTimeout(( () => newNode.destroy()), 200);
            this.node.destroy();
        }else{
            this.node.destroy();
        }
    }
}
