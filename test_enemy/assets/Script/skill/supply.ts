
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    private aimol:cc.Prefab = null;

    @property(cc.Prefab)
    private ar:cc.Prefab = null;

    @property(cc.Prefab)
    private axe:cc.Prefab = null;
    

    @property(cc.Prefab)
    private gun:cc.Prefab = null;

    @property(cc.Prefab)
    private knife:cc.Prefab = null;
    
    @property(cc.Prefab)
    private samura:cc.Prefab = null;
    
    @property(cc.Prefab)
    private shield:cc.Prefab = null;
    
    @property(cc.Prefab)
    private shotgum:cc.Prefab = null;
    
    @property(cc.Prefab)
    private sniper:cc.Prefab = null;

    @property(cc.Prefab)
    private lasergun:cc.Prefab = null;

    @property(cc.Prefab)
    private grenade:cc.Prefab = null;

    @property(cc.AudioClip)
    private supply_effect:cc.AudioClip = null;

    private summon;


    // onLoad () {}

    start () {
        cc.audioEngine.playEffect(this.supply_effect,false);
        this.schedule(()=>{
            let num = this.random(0, 11);
            switch(num){
                case 0:
                    this.summon = cc.instantiate(this.aimol);
                    //cc.log("aimol");
                    break;
                case 1:
                    this.summon = cc.instantiate(this.ar);
                    //cc.log("ar");
                    break;
                case 2:
                    this.summon = cc.instantiate(this.axe);
                    //cc.log("axe")
                    break;
                case 3:
                    this.summon = cc.instantiate(this.lasergun);
                    //cc.log("firegun")
                    break;
                case 4:
                    this.summon = cc.instantiate(this.gun);
                    //cc.log("gun");
                    break;
                case 5:
                    this.summon = cc.instantiate(this.knife);
                    break;
                case 6:
                    this.summon = cc.instantiate(this.samura);
                    //cc.log("samura");
                    break;
                case 7:
                    this.summon = cc.instantiate(this.shield);
                    //cc.log("shield");
                    break;
                case 8:
                    this.summon = cc.instantiate(this.shotgum);
                    //cc.log("shotgum");
                    break;
                case 9:
                    this.summon = cc.instantiate(this.sniper);
                    //cc.log("sniper");
                    break;
            }
            this.summon.setParent(this.node.parent);
            this.summon.setPosition(this.node.position);
            this.scheduleOnce(()=>{this.node.destroy()}, 0.5);
        },5)
    }

    random(lower, upper) {
        return Math.round(Math.random()*(upper - lower));
    }
    // update (dt) {}
}
