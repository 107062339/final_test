const {ccclass, property} = cc._decorator;

@ccclass
export default class datactrl extends cc.Component {

    @property(cc.Prefab)
    private op:cc.Prefab = null;

    @property(cc.Prefab)
    private stage1:cc.Prefab = null;

    @property(cc.Prefab)
    private stage2:cc.Prefab = null;

    @property(cc.Prefab)
    private ninja_house:cc.Prefab = null;

    @property(cc.Prefab)
    private wizard_house:cc.Prefab = null;

    @property(cc.Prefab)
    private ed:cc.Prefab = null;

    @property(cc.Prefab)
    private init_scene:cc.Prefab = null;

    private HP_max = 100;
    
    public username= "";

    public aimol_num;

    public bullet_num;

    public now_stage = "init_scene";

    public weaponui = null;
    
    public weapon1: string = "fist";
    public weapon2: string = "fist";
    public aimol1: number = 0;
    public aimol2: number = 0;
    public Aimol1: number=30;
    public Aimol2: number=30;

    public life:number = 3;
    public hp:number= 100;

    onLoad () {

    }

    start () {
        this.weaponui = this.node.getChildByName("Main Camera").getChildByName("weaponUI").getComponent("weaponUI");
    }

    change_scene(next_stage){
        let newstage = null;
        if(this.now_stage != "init_scene" && this.now_stage != "op"){
            this.get_data();
        }
        console.log(next_stage);
        this.node.getChildByName(this.now_stage).destroy();
        if(next_stage == "op"){
            newstage = cc.instantiate(this.op);
            this.now_stage = 'op';
        }else if(next_stage == "stage1"){
            newstage = cc.instantiate(this.stage1);
            this.now_stage = 'stage1';
        }else if(next_stage == "stage2"){
            newstage = cc.instantiate(this.stage2);
            this.now_stage = 'stage2';
        }else if(next_stage == "ninja_house"){
            newstage = cc.instantiate(this.ninja_house);
            this.now_stage = 'ninja_house';
        }else if(next_stage == "wizard_house"){
            newstage = cc.instantiate(this.wizard_house);
            this.now_stage = 'wizard_house';
        }else if(next_stage == "ed"){
            let camera = cc.find("Canvas/Main Camera");
            camera.x = 0;
            camera.y = 0;
            newstage = cc.instantiate(this.ed);
            this.now_stage = 'ed';
            document.getElementById("Cocos2dGameContainer").style.cursor = "default";

        }else if(next_stage == "init_scene"){
            let camera = cc.find("Canvas/Main Camera");
            camera.x = 0;
            camera.y = 0;
            newstage = cc.instantiate(this.init_scene);
            this.now_stage = 'init_scene';
            
        }
        newstage.parent = this.node;
        if(this.now_stage == "init_scene"){
            setTimeout(()=>{
                newstage.getComponent("init_scene").newandload();
                document.getElementById("Cocos2dGameContainer").style.cursor = "default";
                console.log("new_but");
            }, 100);
        }
        if(this.now_stage != "op" && this.now_stage != "ed" && this.now_stage != "init_scene"){
            newstage.setSiblingIndex(0);
        }
        
        setTimeout(() =>{
            //console.log("setUI");
            if(this.now_stage != "init_scene" && this.now_stage != "op" && this.now_stage != "ed"){
                this.set_data();
                this.data_update();
            }
        }, 100);  
    }

    get_data(){
        let move = cc.find("Canvas/" + this.now_stage + "/player").getComponent("move");
        let HP = cc.find("Canvas/" + this.now_stage + "/player").getComponent("HP");
        this.hp = HP.hp;
        this.life = HP.life;
        this.weapon1 = move.weapon1;
        this.weapon2 = move.weapon2;
        this.aimol1 = move.aimol1;
        this.aimol2 = move.aimol2;
        this.Aimol1 = move.Aimol1;
        this.Aimol2 = move.Aimol2;
    }

    set_data(){
        console.log(this.now_stage);
        let move = cc.find("Canvas/" + this.now_stage + "/player").getComponent("move");
        let HP = cc.find("Canvas/" + this.now_stage + "/player").getComponent("HP");
        let attack = cc.find("Canvas/" + this.now_stage + "/player").getComponent("attack");
        let weaponui = cc.find("Canvas/Main Camera/weaponUI").getComponent("weaponUI");
        HP.hp = this.hp;
        console.log("this and HP", this.hp, HP.hp)
        HP.life = this.life;
        move.weapon1 = this.weapon1;
        move.weapon2 = this.weapon2;
        move.aimol1 = this.aimol1;
        move.aimol2 = this.aimol2;
        move.Aimol1 = this.Aimol1;
        move.Aimol2 = this.Aimol2;
        move.takeWeapon(this.weapon2);
        move.takeWeapon(this.weapon1);
        weaponui.setUI();
        //attack.attackInit();
    }   

    data_update(){
        let self = this;
        console.log(self.username);
        console.log("data_update");
        firebase.database().ref('/username/' + self.username).update({
            life: self.life,
            hp: self.hp,
            weapon1: self.weapon1,
            weapon2: self.weapon2,
            aimo1: self.aimol1,
            aimo2: self.aimol2,
            Aimo1: self.Aimol1,
            Aimo2: self.Aimol2,
            stage: self.now_stage,
        })
    }

    update (dt) {}
}
