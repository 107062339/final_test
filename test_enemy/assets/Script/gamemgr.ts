
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {


    @property(cc.Prefab)
    private safety_mask:cc.Prefab = null;

    @property(cc.Prefab)
    private healing:cc.Prefab = null;

    @property(cc.Prefab)
    private eye:cc.Prefab = null;

    @property(cc.Prefab)
    private air_supply:cc.Prefab = null;


    // LIFE-CYCLE CALLBACKS
    private player;

    private move;

    private attack;

    private mouse;

    private bigger;

    private smaller;

    private max_size = 60;

    private min_size = 30;

    private mouse_init_pos;

    private mouse_end_pos;

    private camera = null;

    private map_height;

    private map_width;



    onLoad () {
    cc.director.getPhysicsManager().enabled = true;
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    this.node.on(cc.Node.EventType.MOUSE_MOVE, this.mouseMove, this);
    this.node.on(cc.Node.EventType.MOUSE_DOWN, this.mouseDown, this);
    this.node.on(cc.Node.EventType.MOUSE_UP, this.mouseUp, this);    
    }

    start () {setTimeout(()=>{
        this.camera = cc.find("Canvas/Main Camera");
        this.player = this.node.getChildByName("player");
        this.move = this.player.getComponent('move');
        this.attack = this.player.getComponent('attack');
        this.mouse = this.node.getChildByName("aim");
        this.mouse_init_pos = cc.v2(0, 0);

        this.bigger = (size) => {
            if(this.mouse.width + 0.2*size < this.max_size){
                this.mouse.width += 0.2*size;
                this.mouse.height += 0.2*size;
            }else{
                this.mouse.size = this.max_size;
            }
        }
        this.smaller = () => {
            if(this.mouse.width > this.min_size){
                this.mouse.width -= 1;
                this.mouse.height -= 1;
            }
        }
        this.schedule(this.player_rotate, 0.01);
        this.schedule(this.smaller, 0.03);
    },200);
    }
    onKeyDown(event){
        switch(event.keyCode)
        {   
            case cc.macro.KEY.w:
                this.move.movedir('w');
                break;
            case cc.macro.KEY.s:
                this.move.movedir('s');
                break;
            case cc.macro.KEY.a:
                this.move.movedir('a');
                break;
            case cc.macro.KEY.d:
                this.move.movedir('d');
                break;
            case cc.macro.KEY.space:
                if(!this.player.getComponent("attack").checkBusy())
                {
                    this.move.Pick();
                }
                break;
            case cc.macro.KEY.q:
                if(!this.player.getComponent("attack").checkBusy())
                {
                    this.move.swapWeapon();
                }
                break;
            case cc.macro.KEY.e:
                if(!this.player.getComponent("attack").checkBusy())
                {
                    this.move.drop(this.player.position);
                }
                break;
            case cc.macro.KEY.r:
                if(cc.isValid(this.player.getComponent("attack"))){
                    if(!this.player.getComponent("attack").checkBusy())
                    {
                        this.move.reloadAimol();
                    }
                }
                break;
            case cc.macro.KEY.t:
                this.make_mask();
                break;
            case cc.macro.KEY.g:
                this.make_heal();
                break;
            case cc.macro.KEY.y:
                this.make_eye();
                break;
            case cc.macro.KEY.f:
                this.make_supply();
                break;
            case cc.macro.KEY.b:
                this.move.reborn();
                break;
        }
    }
    onKeyUp(event){
        switch(event.keyCode)
        {
            case cc.macro.KEY.w:
                this.move.movedir_b('w');
                break;
            case cc.macro.KEY.s:
                this.move.movedir_b('s');
                break;
            case cc.macro.KEY.a:
                this.move.movedir_b('a');
                break;
            case cc.macro.KEY.d:
                this.move.movedir_b('d');
                break;
            case cc.macro.KEY.space:
                this.move.Unpick();
                break;
        }
    }
    
    make_mask(){
        let mask = this.node.getChildByName("player").getChildByName("safety_mask").getComponent("safety_mask");
        if(mask.open == false && mask.break == false){
            cc.log("open");
            mask.mask_open();
        }else if (mask.open == true){
            cc.log("close");
            mask.mask_close();
        }
    }
    make_heal(){
        if(!cc.find("Canvas/" + this.node.name + "/player/healing")){
            let hp = this.node.getChildByName("player").getComponent("HP");
            let heal = cc.instantiate(this.healing);
            heal.parent = cc.find("Canvas/" + this.node.name + "/player");
            hp.heal();
        }
    }
    make_eye(){
        if(!cc.find("Canvas/Main Camera/eye_tp1")){
            let sharingun = cc.instantiate(this.eye);
            sharingun.parent = cc.find("Canvas/Main Camera");
        }
    }
    make_supply(){
        if(!cc.find("Canvas/" + this.node.name + "/supply")){
            let supply = cc.instantiate(this.air_supply);
            cc.log("supply!!");
            supply.parent = this.node;
            supply.x = this.node.getChildByName("aim").x;
            supply.y = this.node.getChildByName("aim").y;
        }
    }

    player_rotate(){
        let startpos = this.player.position;
        //startpos.x += 480;//將cavas裡面的座標轉換成0,0在左下角的座標
        //startpos.y += 320;
        
        let dirVec = startpos;
        dirVec.x = this.mouse_end_pos.x - startpos.x - this.node.width/2 + this.camera.x;
        dirVec.y = this.mouse_end_pos.y - startpos.y - this.node.height/2 + this.camera.y;
        let comVec = new cc.Vec2(0, 1);
        let radian = dirVec.signAngle(comVec);
        let degree = Math.floor(cc.misc.radiansToDegrees(radian));
        //console.log("x角度：" + (degree-90));
        this.move.rotate(degree-90);
        
        this.mouse.x = this.mouse_end_pos.x - this.node.width/2 + this.camera.x;
        this.mouse.y = this.mouse_end_pos.y - this.node.height/2 + this.camera.y;
    }

    mouseMove(event){
        this.mouse_end_pos = event.getLocation();
        if(this.mouse_end_pos.x != this.mouse_init_pos.x || this.mouse_end_pos.y != this.mouse_init_pos.y){
            let diff = this.mouse_end_pos.sub(this.mouse_init_pos);
            this.bigger( Math.sqrt(diff.x * diff.x + diff.y * diff.y));

            this.mouse_init_pos.x = this.mouse_end_pos.x;
            this.mouse_init_pos.y = this.mouse_end_pos.y;
        }
        
    }
    mouseDown(event){
        this.attack = this.player.getComponent('attack');
        if(cc.isValid(this.attack)){
            this.attack.attack(this.move.checkWeapon(), this.mouse_end_pos, this.mouse.width);
            this.move.checkAimol1();
        }
    }
    mouseUp(event){
        //cc.log(this.move.checkWeapon())
         if(cc.isValid(this.move))
         {  if(this.move.checkWeapon()=="lasergun"){
                this.attack.turnOffLasergun();
            }
        }
    }
    update (dt) {
        if(this.player.x > -(this.node.width/2 - 480) && this.player.x < (this.node.width/2 - 480)){
            this.camera.x = this.player.x + 160;
            
        }/*else if(this.player.x >= this.node.width/2 - 480){
            this.camera.x = this.node.width/2 - 240;
        }*/
        if(this.player.y > -(this.node.height/2 - 320) && this.player.y < (this.node.height/2 - 320)){
            this.camera.y = this.player.y + 320;
        }/*else if(this.player.y >= (this.node.height/2 - 320)){
            this.camera.y = this.node.height/2 - 160;
        }*/
        
    }
}
