
const {ccclass, property} = cc._decorator;

@ccclass
export default class player_attack extends cc.Component {

    @property({type:cc.Prefab})
    attack_axe: cc.Prefab = null;

    @property({type:cc.Prefab})
    attack_knife: cc.Prefab = null;
    
    @property({type:cc.Prefab})
    attack_shield: cc.Prefab = null;

    @property({type:cc.Prefab})
    attack_samura: cc.Prefab = null;

    @property({type:cc.Prefab})
    attack_grenade: cc.Prefab = null;
    
    @property({type:cc.Prefab})
    attack_bullet: cc.Prefab = null;
    
    @property({type:cc.Prefab})
    attack_punchl: cc.Prefab = null;

    @property({type:cc.Prefab})
    attack_punchr: cc.Prefab = null;

    @property({type:cc.Prefab})
    attack_laser: cc.Prefab = null;

    @property({type:cc.Prefab})
    prelaser: cc.Prefab = null;

    @property({type:cc.AudioClip})
    punchSound: cc.AudioClip = null;
    
    @property({type:cc.AudioClip})
    knifeSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    samuraSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    shieldSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    axeSound: cc.AudioClip = null;
    
    @property({type:cc.AudioClip})
    gunSound: cc.AudioClip = null;
    
    @property({type:cc.AudioClip})
    sniperSound: cc.AudioClip = null;
    
    @property({type:cc.AudioClip})
    shotgumSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    aimolSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    laserSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    arSound: cc.AudioClip = null;

    private hand_l;
    private hand_r;
    private weapon;
    private weaponInfo;
    private gamemgr;
    private isPunching:boolean=false;
    private isBlading:boolean =false;
    private isKnifing:boolean =false;
    private isShielding:boolean =false;
    private isShooting:boolean = false;
    private startPos_hand_l:cc.Vec2;
    private startPos_hand_r:cc.Vec2;
    private isReloading:boolean=false;
    private isLasering:boolean=false;
    private laserEndPos:cc.Vec2;
    private count:number =0 ;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        setTimeout(()=>{
            this.hand_l = this.node.getChildByName("hand_2_l");
            this.hand_r = this.node.getChildByName("hand_2_r");
            this.weaponInfo = this.node.getComponent("move");
            this.gamemgr = this.node.parent.getComponent("gamemgr");
    
            this.startPos_hand_l = this.hand_l.position;
            this.startPos_hand_r = this.hand_r.position;
        },100);
        
        cc.director.getCollisionManager().enabled = true;
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getPhysicsManager().gravity = cc.v2(0, 0); 

    }

    attackInit(){
        this.hand_l = this.node.getChildByName("hand_2_l");
        this.hand_r = this.node.getChildByName("hand_2_r");
        this.weaponInfo = this.node.getComponent("move");
        this.gamemgr = this.node.parent.getComponent("gamemgr");

        this.startPos_hand_l = this.hand_l.position;
        this.startPos_hand_r = this.hand_r.position;
    }

    attack(weapon:string, endpos, aimSize){
        cc.log(weapon);
        cc.log(endpos);
        if(weapon=="fist"&&!this.isPunching){
            this.isPunching = true;
            cc.log("Punch");
            let fist = Math.floor(Math.random()*2);
            cc.audioEngine.playEffect(this.punchSound, false);
            if( fist == 0 ){
                var seq = cc.sequence(
                    cc.moveTo(0.1, this.startPos_hand_l.x+30, this.startPos_hand_l.y-10).easing(cc.easeInOut(2)),
                    cc.moveTo(0.3, this.startPos_hand_l.x, this.startPos_hand_l.y).easing(cc.easeInOut(2))
                );
                this.hand_l.runAction(seq);
                var newNode:cc.Node;
                this.scheduleOnce(()=>{
                    newNode = cc.instantiate(this.attack_punchl);
                    newNode.setParent(this.node);
                    newNode.setPosition(this.hand_l.x-10, this.hand_l.y-10);//todo:拳風位置
                    newNode.getComponent("melee").init(5);
                   // newNode.getComponent(cc.RigidBody).syncPosition(true);
                }, 0.05);
                this.scheduleOnce(()=>{
                    newNode.destroy();
                }, 0.25);
            }else if(fist == 1){
                var seq = cc.sequence(
                    cc.moveTo(0.1, this.startPos_hand_r.x+30, this.startPos_hand_r.y+10).easing(cc.easeInOut(2)),
                    cc.moveTo(0.3, this.startPos_hand_r.x, this.startPos_hand_r.y).easing(cc.easeInOut(2))
                );
                var newNode:cc.Node;
                this.scheduleOnce(()=>{
                    newNode = cc.instantiate(this.attack_punchr);
                    newNode.setParent(this.node);
                    newNode.setPosition(this.hand_r.x-10, this.hand_r.y+10);
                    newNode.getComponent("melee").init(5);
                    //newNode.getComponent(cc.RigidBody).syncPosition(true);
                }, 0.05);
                this.scheduleOnce(()=>{
                    newNode.destroy();
                }, 0.25);
                this.hand_r.runAction(seq);
            }
            setTimeout(( () => this.isPunching = false ), 400);
        }else{
            cc.log(weapon);
            this.weapon = this.node.getChildByName(weapon);
            cc.log(this.weapon, this.node);
            if(weapon=="samura"&&!this.isBlading){
                this.isBlading = true;
                cc.audioEngine.playEffect(this.samuraSound, false);
                var mov = cc.sequence(
                    cc.spawn(
                        cc.moveBy(0.2, 90, -60).easing(cc.easeInOut(2)),
                        cc.rotateBy(0.2, 150).easing(cc.easeInOut(2)),
                    ),
                    cc.spawn(
                        cc.moveBy(0.4, -90, 60).easing(cc.easeInOut(2)),
                        cc.rotateBy(0.4, -150).easing(cc.easeInOut(2))
                    ));
                var mov1 = cc.sequence(
                    cc.spawn(
                        cc.moveBy(0.2, 30, -30).easing(cc.easeInOut(2)),
                        cc.rotateBy(0.2, 150).easing(cc.easeInOut(2)),
                    ),
                    cc.spawn(
                        cc.moveBy(0.399, -30, 30).easing(cc.easeInOut(2)),
                        cc.rotateBy(0.399, -150).easing(cc.easeInOut(2))
                    ));
                var mov2 = cc.sequence(
                    cc.spawn(
                        cc.moveBy(0.2, 10, -20).easing(cc.easeInOut(2)),
                        cc.rotateBy(0.2, 150).easing(cc.easeInOut(2)),
                    ),
                    cc.spawn(
                        cc.moveBy(0.399, -10, 20).easing(cc.easeInOut(2)),
                        cc.rotateBy(0.399, -150).easing(cc.easeInOut(2))
                    ));
                this.scheduleOnce(()=>{
                    newNode = cc.instantiate(this.attack_samura);
                    newNode.setParent(this.node);
                    newNode.setPosition(this.hand_r.x+30, this.hand_r.y);
                    this.scheduleOnce(()=>{
                        newNode.destroy();
                    }, 0.2);
                }, 0.05);
                //newNode.getComponent(cc.RigidBody).syncPosition(true);
                this.weapon.runAction(mov);
                this.hand_l.runAction(mov1);
                this.hand_r.runAction(mov2);
                setTimeout(( () => this.isBlading = false ), 800);
            }
            else if(weapon=="knife"&&!this.isKnifing){
                this.isKnifing = true;
                cc.audioEngine.playEffect(this.knifeSound, false);
                var seq = cc.sequence(
                    cc.moveBy(0.1, 10, 0).easing(cc.easeInOut(2)),
                    cc.moveBy(0.3, -10, 0).easing(cc.easeInOut(2))
                );
                var seq1 = cc.sequence(
                    cc.moveBy(0.1, 10, 0).easing(cc.easeInOut(2)),
                    cc.moveBy(0.3, -10, 0).easing(cc.easeInOut(2))
                );
                var seq2 = cc.sequence(
                    cc.moveBy(0.1, 10, 0).easing(cc.easeInOut(2)),
                    cc.moveBy(0.3, -10, 0).easing(cc.easeInOut(2))
                );
                    newNode = cc.instantiate(this.attack_knife);
                    newNode.setParent(this.node);
                    newNode.setPosition(this.hand_r.x+30, this.hand_r.y);
                    //newNode.getComponent(cc.RigidBody).syncPosition(true);
                newNode.runAction(seq2);
                this.scheduleOnce(()=>{
                    newNode.destroy();
                }, 0.2);
                this.hand_r.runAction(seq);
                this.weapon.runAction(seq1);
                this.isKnifing = true;
                setTimeout(( () => this.isKnifing = false ), 400);
            }
            else if(weapon=="shield"&&!this.isShielding){
                this.isShielding = true;
                cc.audioEngine.playEffect(this.shieldSound, false);
                var seq = cc.sequence(
                    cc.moveBy(0.1, 10, 0).easing(cc.easeInOut(2)),
                    cc.moveBy(0.3, -10, 0).easing(cc.easeInOut(2))
                );
                var seq1 = cc.sequence(
                    cc.moveBy(0.1, 10, 0).easing(cc.easeInOut(2)),
                    cc.moveBy(0.3, -10, 0).easing(cc.easeInOut(2))
                );
                var seq2 = cc.sequence(
                    cc.moveBy(0.1, 10, 0).easing(cc.easeInOut(2)),
                    cc.moveBy(0.3, -10, 0).easing(cc.easeInOut(2))
                );
                this.hand_r.runAction(seq);
                this.weapon.runAction(seq1);
                newNode = cc.instantiate(this.attack_shield);
                newNode.setParent(this.node);
                newNode.setPosition(this.hand_r.x+15, this.hand_r.y+5);
                newNode.runAction(seq2);
                this.scheduleOnce(()=>{
                    newNode.destroy();
                }, 0.2);
                this.isShielding = true;
                setTimeout(( () => {
                    this.isShielding = false; 
                }), 400);
            }else if(weapon=="axe"){
                cc.audioEngine.playEffect(this.axeSound, false);
                var newNode = cc.instantiate(this.attack_axe);
                newNode.setParent(this.node.parent);
                newNode.setPosition(this.node.position);
                this.weaponInfo.drop(null);
                cc.log(this.weaponInfo);
                
                //newNode.getComponent("axe").throw();
            }else if(weapon=="grenade"){
                //cc.audioEngine.playEffect(this.axeSound, false); todp:set sound
                var newNode = cc.instantiate(this.attack_grenade);
                newNode.setParent(this.node.parent);
                newNode.setPosition(this.node.position);
                this.weaponInfo.drop(null);
                cc.log(this.weaponInfo);
                
                //newNode.getComponent("axe").throw();
            }else if(weapon=="grenade"){
                cc.audioEngine.playEffect(this.axeSound, false);
                var newNode = cc.instantiate(this.attack_grenade);
                newNode.setParent(cc.find("Canvas"));
                newNode.setPosition(this.node.position);
                this.weaponInfo.drop(null);
                cc.log(this.weaponInfo);
                
                //newNode.getComponent("axe").throw();
            }else if(weapon=="gun"){
                if(this.weaponInfo.checkAimol1()>0&&!this.isShooting&&!this.isReloading){
                    cc.audioEngine.playEffect(this.gunSound, false);
                    this.isShooting = true;
                    this.weaponInfo.consumeAimol(1);
                    var newNode = cc.instantiate(this.attack_bullet);
                    newNode.setParent(this.node.parent);
                    newNode.setPosition(this.node.position);
                    newNode.getComponent("bullet").init(this.node, this.weapon, aimSize, 1, 8);
                    this.gamemgr.bigger(20);
                    setTimeout(( () => this.isShooting = false ), 300);
                }
            }else if(weapon=="shotgum"){
                if(this.weaponInfo.checkAimol1()>0&&!this.isShooting&&!this.isReloading){
                    cc.audioEngine.playEffect(this.shotgumSound, false);
                    this.isShooting = true;
                    let temp = this.weaponInfo.checkAimol1();
                    this.weaponInfo.consumeAimol(temp);
                    for(var i = 0; i<5; i++){
                        var newNode = cc.instantiate(this.attack_bullet);
                        newNode.setParent(this.node.parent);
                        newNode.setPosition(this.node.position);
                        newNode.getComponent("bullet").init(this.node, this.weapon, aimSize*4  , 1, 20);
                    }
                    setTimeout(( () => this.isShooting = false ), 300);
                    cc.log("weapon name:"+this.weapon.name);
                }
            }else if(weapon=="ar"){
                if(this.weaponInfo.checkAimol1()>0&&!this.isShooting&&!this.isReloading){
                    cc.audioEngine.playEffect(this.arSound, false);
                    this.isShooting = true;
                    this.weaponInfo.consumeAimol(1);
                    var newNode = cc.instantiate(this.attack_bullet);
                    newNode.setParent(this.node.parent);
                    newNode.setPosition(this.node.position);
                    newNode.getComponent("bullet").init(this.node, this.weapon, aimSize, 1.5, 10);
                    setTimeout(( () => this.isShooting = false ), 100);
                    this.gamemgr.bigger(30);
                    cc.log("weapon name:"+this.weapon.name);
                }
            }else if(weapon=="sniper"){
                if(this.weaponInfo.checkAimol1()>0&&!this.isShooting&&!this.isReloading){
                    cc.audioEngine.playEffect(this.sniperSound, false);
                    this.isShooting = true;
                    this.weaponInfo.consumeAimol(1);
                        var newNode = cc.instantiate(this.attack_bullet);
                        newNode.setParent(this.node.parent);
                        newNode.setPosition(this.node.position);
                        newNode.getComponent("bullet").init(this.node, this.weapon, aimSize, 2, 50);
                    setTimeout(( () => this.isShooting = false ), 0);
                    this.gamemgr.bigger(30);
                    cc.log("weapon name:"+this.weapon.name);
                }
            }else if(weapon=="lasergun"){
                if(this.weaponInfo.checkAimol1()>0&&!this.isShooting&&!this.isReloading){
                    cc.audioEngine.playEffect(this.laserSound, false);
                    this.isShooting = true;
                    this.isLasering = true;
                    this.count = 0;
                    this.weaponInfo.consumeAimol(1);
                    var newNode = cc.instantiate(this.prelaser);
                    newNode.opacity = 0;
                    newNode.setParent(this.node.parent);
                    newNode.setPosition(this.node.position);
                    newNode.getComponent("bullet").init(this.node, this.weapon, 0, 100, 0.5);
                    this.scheduleOnce(()=>this.schedule(this.callback, 0.005, Infinity, 0),0.5);
                    cc.log("weapon name:"+this.weapon.name);
                }
            }
        }
    }

    callback(){
        if (!this.isLasering||this.weaponInfo.checkAimol1()<=0) {
            setTimeout(( () => this.isShooting = false ), 300);
            this.unschedule(this.callback);
        }
        cc.log(this.count);
        this.count++;
            if(this.count%10==0) this.weaponInfo.consumeAimol(1);
            this.shootLaser();
    }
    
    shootLaser(){
        var newNode = cc.instantiate(this.prelaser);
        newNode.opacity = 0;
        newNode.setParent(this.node.parent);
        newNode.setPosition(this.node.position);
        newNode.getComponent("bullet").init(this.node, this.weapon, 0, 100, 0.5);
        let tempPos ;
        let tempDiff ;
        let tempSize ;
            let dist = Math.sqrt(this.weapon.position.x * this.weapon.position.x + this.weapon.position.y * this.weapon.position.y);
            let x = dist * Math.cos( this.node.rotation * ( Math.PI / 180));
            let y = dist * -1 * Math.sin( this.node.rotation * ( Math.PI / 180));
            console.log("a");
            tempDiff = this.laserEndPos.sub(this.node.position.add(cc.v2(x, y)));
            tempSize = Math.sqrt(tempDiff.x * tempDiff.x + tempDiff.y * tempDiff.y);
            tempPos= this.node.position.add(this.laserEndPos);
            tempPos.divSelf(2);
            console.log("b");
            let dir = this.laserEndPos.sub(this.node.position);
            dir.normalizeSelf();
            console.log("c");
            let degree = cc.misc.radiansToDegrees(dir.signAngle(cc.v2(1,0)));
            //cc.log("laser info",tempPos, tempSize, degree);
            var newLaser = cc.instantiate(this.attack_laser);
            //newNode.opacity = 0;
            cc.log(newLaser);
            newLaser.setParent(this.node.parent);
            newLaser.setPosition(tempPos);
            newLaser.setContentSize(tempSize, 10);
            newLaser.setRotation(degree);
            //cc.log(newLaser);
            this.scheduleOnce(function(){newLaser.destroy();}, 0.004);

    }
    

    setLaserEndPos(Endpos){
        this.laserEndPos = Endpos; 
        cc.log("Endpos:"+this.laserEndPos);
    }

    beReloading(weapon:string){
        let reloadTime = 0;
        this.hand_l = this.node.getChildByName("hand_2_l");
        this.hand_r = this.node.getChildByName("hand_2_r");
        this.weaponInfo = this.node.getComponent("move");
        this.weapon = this.node.getChildByName(weapon);
        if(!(cc.isValid(this.weapon)&&cc.isValid(this.hand_l)&&cc.isValid(this.hand_r))){
            return;
        }
        if(weapon == "ar"){
             reloadTime = 750; 
        }else if(weapon == "lasergun"){
             reloadTime = 1000; 
        }else if(weapon == "gun"){
             reloadTime = 750; 
        }else if(weapon == "shotgum"){
             reloadTime = 1000; 
        }else if(weapon == "sniper"){
             reloadTime = 750; 
        }else{
             reloadTime = 0; 
        }
        if(weapon=="gun"||weapon=="shotgum"||weapon=="lasergun"||weapon=="sniper"||weapon=="ar"&&!this.isReloading)
        {
            let id = cc.audioEngine.playEffect(this.aimolSound, false);
            this.isReloading = true;
            var mov = cc.sequence(
                cc.spawn(
                    cc.moveBy(0.25, 10, -10).easing(cc.easeInOut(2)),
                    cc.rotateBy(0.25, 80).easing(cc.easeInOut(2)),
                ),
                cc.spawn(
                    cc.moveBy(0.25, -10, 10).easing(cc.easeInOut(2)),
                    cc.rotateBy(0.25, -80).easing(cc.easeInOut(2))
                ));
            var mov1 = cc.sequence(
                cc.spawn(
                    cc.moveBy(0.25, 10, -10).easing(cc.easeInOut(2)),
                    cc.rotateBy(0.25, 80).easing(cc.easeInOut(2)),
                ),
                cc.spawn(
                    cc.moveBy(0.25, -10, 10).easing(cc.easeInOut(2)),
                    cc.rotateBy(0.25, -80).easing(cc.easeInOut(2))
                ));
            var mov2 = cc.sequence(
                cc.spawn(
                    cc.moveBy(0.25, -10, 20).easing(cc.easeInOut(2)),
                    cc.rotateBy(0.25, -80).easing(cc.easeInOut(2)),
                ),
                cc.spawn(
                    cc.moveBy(0.25, 10, -20).easing(cc.easeInOut(2)),
                    cc.rotateBy(0.25, 80).easing(cc.easeInOut(2))
                ));
                var mov2long = cc.sequence(
                    cc.spawn(
                        cc.moveBy(0.25, -5, 10).easing(cc.easeInOut(2)),
                        cc.rotateBy(0.25, -40).easing(cc.easeInOut(2))
                    ),
                    cc.spawn(
                        cc.moveBy(0.25, 5, -10).easing(cc.easeInOut(2)),
                        cc.rotateBy(0.25, 40).easing(cc.easeInOut(2))
                    )
                );
                this.weapon.runAction(mov);
                this.hand_l.runAction(mov2);
                this.hand_r.runAction(mov1);
                if(reloadTime>=1000){
                    this.hand_l.runAction(mov2long);
                }
                setTimeout(( () => {
                    this.isReloading = false;
                    cc.audioEngine.stopEffect(id);
                }), reloadTime);

        }
    }

    turnOffLasergun(){
        this.isLasering = false;
    }

    checkBusy(){
        return this.isBlading||this.isKnifing||this.isShooting||this.isPunching||this.isReloading;
    }

    //update (dt) {}
}
