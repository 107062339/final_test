// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    blood: cc.Prefab = null;

    @property()
    damage: number = 5;

    start () {
        
    }

    init(damage:number){
        this.damage = damage;
    }

    onBeginContact(contact, selfCollider, otherCollider)
    {
        cc.log(selfCollider.node.name, otherCollider.node.group);
        if(otherCollider.node.group == "enemy"&&selfCollider.node.name!="shield"){
            var newNode = cc.instantiate(this.blood);
            let dirx = Math.floor(Math.random()*2);
            dirx = (dirx == 1)? 1: -1;
            let diry = Math.floor(Math.random()*2);
            diry = (diry == 1)? 1: -1;
            let posx = Math.floor(Math.random()*25);
            let posy = Math.floor(Math.random()*25);
            let rotation = Math.floor(Math.random()*360);
            let scalar = 0.5 + Math.random()*0.5;
            newNode.setParent(otherCollider.node);
            newNode.setPosition(dirx*posx, diry*posy);
            newNode.setRotation(rotation);
            newNode.setScale(scalar);
            setTimeout(( () => newNode.destroy()), 300);
        }else if(selfCollider.node.name == "shield"&&otherCollider.node.group == "ene_bullet"){
            otherCollider.node.destroy();
        }
    }

    // update (dt) {}
}
