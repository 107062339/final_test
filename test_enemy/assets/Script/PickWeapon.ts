const {ccclass, property} = cc._decorator;

@ccclass
export default class PickWeapon extends cc.Component {

    private aimol:number = 0;

    @property({type:cc.AudioClip})
    pickSound: cc.AudioClip = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.director.getCollisionManager().enabled = true;
        cc.director.getPhysicsManager().enabled = true;
        if(this.node.name == "axe"){
            this.aimol = 0;
        }else if(this.node.name == "ar"){
            this.aimol = 15;
        }else if(this.node.name == "clow"){
            this.aimol = 0;
        }else if(this.node.name == "firegun"){
            this.aimol = 3;
        }else if(this.node.name == "gun"){
            this.aimol = 6;
        }else if(this.node.name == "knife"){
            this.aimol = 0;
        }else if(this.node.name == "samura"){
            this.aimol = 0;
        }else if(this.node.name == "shield"){
            this.aimol = 0;
        }else if(this.node.name == "shotgum"){
            this.aimol = 5;
        }else if(this.node.name == "sniper"){
            this.aimol = 1;
        }else if(this.node.name == "grenade"){
            this.aimol = 0;
        }else if(this.node.name == "lasergun"){
            this.aimol = 30;
        }else if(this.node.name == "misslegun"){
            this.aimol = 8;
        }
    }

    start () {
        //cc.log("weapon is ready.");
        var seq = cc.repeatForever(
            cc.sequence(
                cc.moveBy(2, 0, 2).easing(cc.easeInOut(2)),
                cc.moveBy(2, 0, -2).easing(cc.easeInOut(2))
            ));
        //todo: initialize aimol by weapon
        this.node.runAction(seq);
    }

    SetAimol(aimol:number){
        this.aimol = aimol;
    }

    getAimol(){
        return this.aimol;
    }

    Pick() {
        cc.audioEngine.playEffect(this.pickSound, false);
        this.node.destroy();
        //cc.log("You pick "+this.node.name);
    }

    onBeginContact(contact, selfCollider, otherCollider) {
        //cc.log("You touched the weapon.");
        //this.Pick();
    }
    
}
